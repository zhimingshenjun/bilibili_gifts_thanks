# -*- coding: utf-8 -*-
import math
from PySide6.QtWidgets import *
from PySide6.QtGui import *
from PySide6.QtCore import *


class OutlinedLabel(QLabel):
    def __init__(self, text='', fontColor='#FFFFFF', outColor='#FFFFFF'):
        super().__init__()
        self.setText(text)
        self.w = 1 / 10
        self.mode = True
        self.setBrush(QColor(fontColor))
        self.setPen(outColor)

    def scaledOutlineMode(self):
        return self.mode

    def setScaledOutlineMode(self, state):
        self.mode = state

    def outlineThickness(self):
        return self.w * self.font().pointSize() if self.mode else self.w

    def setOutlineThickness(self, value):
        self.w = value

    def setBrush(self, brush):
        if not isinstance(brush, QBrush):
            brush = QBrush(brush)
        self.brush = brush

    def setPen(self, pen):
        if not isinstance(pen, QPen):
            pen = QPen(pen)
        pen.setJoinStyle(Qt.RoundJoin)
        self.pen = pen

    def sizeHint(self):
        w = math.ceil(self.outlineThickness() * 2)
        return super().sizeHint() + QSize(w, w)

    def minimumSizeHint(self):
        w = math.ceil(self.outlineThickness() * 2)
        return super().minimumSizeHint() + QSize(w, w)

    def paintEvent(self, event):
        w = self.outlineThickness()
        rect = self.rect()
        metrics = QFontMetrics(self.font())
        tr = metrics.boundingRect(self.text()).adjusted(0, 0, w, w)
        if self.indent() == -1:
            if self.frameWidth():
                indent = (metrics.boundingRect('x').width() + w * 2) / 2
            else:
                indent = w
        else:
            indent = self.indent()

        if self.alignment() & Qt.AlignLeft:
            x = rect.left() + indent - min(metrics.leftBearing(self.text()['0']), 0)
        elif self.alignment() & Qt.AlignRight:
            x = rect.x() + rect.width() - indent - tr.width()
        else:
            x = (rect.width() - tr.width()) / 2

        if self.alignment() & Qt.AlignTop:
            y = rect.top() + indent + metrics.ascent()
        elif self.alignment() & Qt.AlignBottom:
            y = rect.y() + rect.height() - indent - metrics.descent()
        else:
            y = (rect.height() + metrics.ascent() - metrics.descent()) / 2

        path = QPainterPath()
        path.addText(x, y, self.font(), self.text())
        qp = QPainter(self)
        qp.setRenderHint(QPainter.Antialiasing)

        self.pen.setWidthF(w * 2)
        qp.strokePath(path, self.pen)
        if 1 < self.brush.style() < 15:
            qp.fillPath(path, self.palette().window())
        qp.fillPath(path, self.brush)


class GIFWidget(QWidget):
    finish = Signal()
    moveDelta = Signal(QPoint)

    def __init__(self, qmovie, opacity=False, top=False, frame=180,
                 fontColor='#000000', outColor='#FFFFFF', parent=None):
        super().__init__(parent)
        self.mousePressToken = False
        self.executeToken = False
        self.ID = 'DD'
        self.number = '100'
        self.gift = '小心心'
        self.price = '10'
        self.setWindowTitle('答谢特效')
        if opacity:
            self.setAttribute(Qt.WA_TranslucentBackground)
        self.setWindowFlag(Qt.FramelessWindowHint)
        if top:
            self.setWindowFlag(Qt.WindowStaysOnTopHint)
        self.show()
        self.layout = QGridLayout()
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(20, 0, 20, 20)
        self.setLayout(self.layout)

        # self.text = ' 感谢 DD 投喂的100个小心心 '
        self.text = ''
        self.showText = OutlinedLabel(self.text, fontColor, outColor)
        self.showText.setAlignment(Qt.AlignCenter)
        self.showText.setAttribute(Qt.WA_TranslucentBackground)
        self.layout.addWidget(self.showText, 1, 0, 1, 1)
        self.textOpacity = QGraphicsOpacityEffect()
        self.showText.setGraphicsEffect(self.textOpacity)
        self.textOpacity.setOpacity(1)

        self.showText2 = OutlinedLabel(self.text, fontColor, outColor)
        self.showText2.setAlignment(Qt.AlignCenter)
        self.showText2.setAttribute(Qt.WA_TranslucentBackground)
        self.layout.addWidget(self.showText2, 2, 0, 1, 1)
        self.textOpacity2 = QGraphicsOpacityEffect()
        self.showText2.setGraphicsEffect(self.textOpacity2)
        self.textOpacity2.setOpacity(1)

        self.showGIF = QLabel()
        self.showGIF.setAttribute(Qt.WA_TranslucentBackground)
        self.showGIF.setAlignment(Qt.AlignCenter)
        self.layout.addWidget(self.showGIF, 0, 0, 1, 1)
        # if gifPath:
        #     self.movie = QMovie(gifPath)
        #     self.showGIF.setMovie(self.movie)
        #     self.movie.start()
        self.showGIF.setMovie(qmovie)
        self.gifOpacity = QGraphicsOpacityEffect()
        self.showGIF.setGraphicsEffect(self.gifOpacity)
        self.gifOpacity.setOpacity(1)

        self.totalFrame = frame
        self.frame = self.totalFrame
        self.initGIFPos = None
        self.initWords1Pos = None
        self.initWords2Pos = None
        self.vyList = [10, 10, 10]
        self.animationTimer = QTimer()
        self.animationTimer.setInterval(16)
        self.animationTimer.timeout.connect(self.playAnimate)

        self.exit = False

        self.origin_x = 0
        self.adjustSizeTimer = QTimer()
        self.adjustSizeTimer.timeout.connect(self.adjust)
        self.adjustSizeTimer.start(100)

    def setGIFPath(self, gifPath):
        self.movie = QMovie(gifPath)
        self.showGIF.setMovie(self.movie)
        self.movie.start()

    def setText(self, text, mode, default=False, line=1, giftInfo=['DD', '100', '小心心', '10']):
        self.ID, self.number, self.gift, self.price = giftInfo
        if line == 1:
            if text:
                self.showText.show()
                self.showText.setText(text.replace('{用户}', self.ID).replace('{数量}', str(self.number)).replace('{礼物}', self.gift).replace('{价格}', str(self.price)).replace('{留言}', self.gift))
            else:
                self.showText.hide()
        elif line == 2:
            if text:
                self.showText2.show()
                self.showText2.setText(text.replace('{用户}', self.ID).replace('{数量}', str(self.number)).replace('{礼物}', self.gift).replace('{价格}', str(self.price)).replace('{留言}', self.gift))
            else:
                self.showText2.hide()

    def setFont(self, font):
        metrics = QFontMetrics(font)
        path = QPainterPath()
        pen = QPen(Qt.red)
        penwidth = 2
        pen.setWidth(penwidth)

        # l = metrics.width(self.text)
        l = metrics.horizontalAdvance(self.text)

        w = self.showText.width()
        px = (l - w) / 2
        if px < 0:
            px = -px
        py =  (self.showText.height() - metrics.height()) / 2 + metrics.ascent()
        if py < 0:
            py = -py
        path.addText(px, py, font, self.text)
        painter = QPainter()
        painter.strokePath(path, pen)
        painter.drawPath(path)
        painter.fillPath(path, QBrush(Qt.red))
        self.showText.setFont(font)

        # l = metrics.width(self.text)
        l = metrics.horizontalAdvance(self.text)
        w = self.showText2.width()
        px = (l - w) / 2
        if px < 0:
            px = -px
        py = (self.showText2.height() - metrics.height()) / 2 + metrics.ascent()
        if py < 0:
            py = -py
        path.addText(px, py, font, self.text)
        painter = QPainter()
        painter.strokePath(path, pen)
        painter.drawPath(path)
        painter.fillPath(path, QBrush(Qt.red))
        self.showText2.setFont(font)

    def setColor(self, color):
        self.showText.setStyleSheet('color:' + color)
        self.showText2.setStyleSheet('color:' + color)

    def setBackgroundColor(self, color):
        self.setStyleSheet('background-color:%s' % color)

    # def setGiftInfo(self, giftInfo):
    #     self.text = ' 感谢 %s 投喂的%s个%s ' % tuple(giftInfo)
    #     self.showText.setText(self.text)

    # def setGuard(self, guardInfo):
    #     self.text = '%s %s %s' % tuple(guardInfo)
    #     self.showText.setText(self.text)

    def setSecond(self, seconds):
        self.frame = seconds * 60
        self.totalFrame = seconds * 60

    def mousePressEvent(self, QEvent):
        self.mousePressToken = True
        self.startPos = QEvent.pos()

    def mouseReleaseEvent(self, QEvent):
        self.mousePressToken = False

    def mouseMoveEvent(self, QEvent):
        if self.mousePressToken:
            self.move(self.pos() + (QEvent.pos() - self.startPos))
            self.moveDelta.emit(QEvent.pos() - self.startPos)

    def playAnimate(self):
        self.adjustSizeTimer.stop()
        opacityList = [self.gifOpacity, self.textOpacity, self.textOpacity2]
        widgets = [self.showGIF, self.showText, self.showText2]
        initPos = [self.initGIFPos, self.initWords1Pos, self.initWords2Pos]
        for index, opacity in enumerate(opacityList):
            if self.frame >= self.totalFrame - 5 - index * 5:
                opacity.setOpacity(0)
                widgets[index].move(initPos[index])
            elif self.frame >= self.totalFrame - 15 - index * 5:
                opacity.setOpacity((self.totalFrame - self.frame + index * 5 - 5) / 10)
                widgets[index].move(widgets[index].pos() - QPoint(0, self.vyList[index]))
                if self.vyList[index] > 1: self.vyList[index] -= 1
            elif self.frame > 40 - index * 5:
                pass
            elif self.frame > -index * 5:
                opacity.setOpacity(self.frame / 40)
                widgets[index].move(widgets[index].pos() - QPoint(0, self.vyList[index]))
                self.vyList[index] += 5
            else:
                self.frame = self.totalFrame
                self.animationTimer.stop()
                self.finish.emit()
                for index, widget in enumerate(widgets):
                    widget.move(initPos[index] - QPoint(0, 55))
                if not self.executeToken:
                    for opacity in opacityList:
                        opacity.setOpacity(1)
                else:
                    for opacity in opacityList:
                        opacity.setOpacity(0)
                self.vyList = [10, 10, 10]
                self.adjustSizeTimer.start()
                break
        self.frame -= 1

    def closeEvent(self, event):
        if not self.exit:
            event.ignore()

    def moveEvent(self, QMoveEvent):
        self.origin_x = self.pos().x() + self.width() * 0.5

    def adjust(self):
        self.adjustSize()
        new_x = self.origin_x - self.width() * 0.5
        self.move(new_x, self.pos().y())
        self.initGIFPos = self.showGIF.pos() + QPoint(0, 55)
        self.initWords1Pos = self.showText.pos() + QPoint(0, 55)
        self.initWords2Pos = self.showText2.pos() + QPoint(0, 55)
