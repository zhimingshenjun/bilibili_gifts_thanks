<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="bilibili_gift_thanks.py" line="180"/>
        <source>option</source>
        <translation type="unfinished">設定</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="181"/>
        <source>keywords</source>
        <translation type="unfinished">キーワード</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="184"/>
        <source>advanced</source>
        <translation type="unfinished">高級設定</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="187"/>
        <source>check update</source>
        <translation type="unfinished">アップデート</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="197"/>
        <source>bilibili live room</source>
        <translation type="unfinished">ビリビリ配信ルーム番号</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="212"/>
        <source>GIFT</source>
        <translation type="unfinished">プレゼント</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="217"/>
        <source>free gift</source>
        <translation type="unfinished">無料プレゼント</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="299"/>
        <source>hide</source>
        <translation type="unfinished">非表示</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="337"/>
        <source>preset</source>
        <translation type="unfinished">設定</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="227"/>
        <source>0.1-10 Yuan</source>
        <translation type="unfinished">0.1-10 人民元</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="237"/>
        <source>10.1-100 Yuan</source>
        <translation type="unfinished">10.1-100 人民元</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="303"/>
        <source>preview</source>
        <translation type="unfinished">効果をプレビュー</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="247"/>
        <source>&gt; 100 Yuan</source>
        <translation type="unfinished">100 人民元以上</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="257"/>
        <source>GUARD</source>
        <translation type="unfinished">大航海</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="307"/>
        <source>display gift</source>
        <translation type="unfinished">プレゼント箱を表示</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="914"/>
        <source>START TRIGGER</source>
        <translation type="unfinished">キャプチャを開始</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="342"/>
        <source>duration</source>
        <translation type="unfinished">表示时间</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="346"/>
        <source> second</source>
        <translation type="unfinished"> 秒</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="351"/>
        <source>picture size</source>
        <translation type="unfinished">画像サイズ</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="647"/>
        <source>click to select or drag in picture/gif to display</source>
        <translation type="unfinished">クリックまたはドラッグで感謝の画像やgifをアップロード</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="373"/>
        <source>words 1</source>
        <translation type="unfinished">感谢のセリフ1</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="380"/>
        <source>words 2</source>
        <translation type="unfinished">感谢のセリフ2</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="387"/>
        <source>font set</source>
        <translation type="unfinished">フォント選択</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="400"/>
        <source>bold</source>
        <translation type="unfinished">太くする</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="406"/>
        <source>font color</source>
        <translation type="unfinished">フォント色</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="413"/>
        <source>font border</source>
        <translation type="unfinished">輪郭色</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="419"/>
        <source>weight</source>
        <translation type="unfinished">輪郭のサイズ</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="427"/>
        <source>sound</source>
        <translation type="unfinished">SE選択</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="433"/>
        <source>volume</source>
        <translation type="unfinished">SE音量</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="575"/>
        <source>select sound effect</source>
        <translation type="unfinished">SEを選択する</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="641"/>
        <source>select picture or gif</source>
        <translation type="unfinished">画像またはgifを選択する</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="889"/>
        <source>STOP TRIGGER</source>
        <translation type="unfinished">キャプチャを停止</translation>
    </message>
    <message>
        <location filename="bilibili_gift_thanks.py" line="1"/>
        <source>max</source>
        <translation type="unfinished">上限</translation>
    </message>
</context>
</TS>
