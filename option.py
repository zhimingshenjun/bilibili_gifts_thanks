# -*- coding: utf-8 -*-
from PySide6.QtWidgets import *
from PySide6.QtCore import *


def _translate(context, text, disambig):
    return QApplication.translate(context, text, disambig)


class KeyWords(QWidget):
    def __init__(self):
        super(KeyWords, self).__init__()
        self.resize(400, 200)
        self.setWindowTitle(self.tr('关键词'))
        layout = QGridLayout()
        self.setLayout(layout)
        layout.addWidget(QLabel('{用户}'), 0, 0)
        layout.addWidget(QLabel(self.tr('转换为用户名')), 0, 1)
        layout.addWidget(QLabel('{数量}'), 1, 0)
        layout.addWidget(QLabel(self.tr('转换为礼物数量')), 1, 1)
        layout.addWidget(QLabel('{礼物}'), 2, 0)
        layout.addWidget(QLabel(self.tr('转换为礼物名')), 2, 1)
        layout.addWidget(QLabel('{价格}'), 3, 0)
        layout.addWidget(QLabel(self.tr('转换为礼物价格')), 3, 1)
        layout.addWidget(QLabel('{留言}'), 4, 0)
        layout.addWidget(QLabel(self.tr('转换为SC留言')), 4, 1)


class OptionWidget(QWidget):
    color = Signal(str)
    opacity = Signal(bool)
    top = Signal(bool)

    def __init__(self, color, opacityToken, topToken):
        super().__init__()
        self.resize(400, 200)
        self.setWindowTitle('高级设置')
        layout = QGridLayout()
        layout.setSpacing(30)
        self.setLayout(layout)
        backgroundColorButton = QPushButton('背景颜色')
        backgroundColorButton.clicked.connect(self.selectBackgroundColor)
        layout.addWidget(backgroundColorButton, 0, 0, 1, 2)
        self.backgroundColorLabel = QLabel(color)
        self.backgroundColorLabel.setStyleSheet('background-color:%s;color:%s' % (color, color))
        layout.addWidget(self.backgroundColorLabel, 0, 2, 1, 2)

        self.opacityButton = QPushButton('透明背景 (需重启)')
        self.opacityButton.clicked.connect(self.setOpacity)
        self.opacityToken = opacityToken
        if self.opacityToken:
            self.opacityButton.setStyleSheet('background-color:#87CEFA')
        else:
            self.opacityButton.setStyleSheet('background-color:white')
        layout.addWidget(self.opacityButton, 1, 0, 1, 2)

        self.stayTopButton = QPushButton('特效置顶 (需重启)')
        self.stayTopButton.clicked.connect(self.setTop)
        self.topToken = topToken
        if self.topToken:
            self.stayTopButton.setStyleSheet('background-color:#87CEFA')
        else:
            self.stayTopButton.setStyleSheet('background-color:white')
        layout.addWidget(self.stayTopButton, 1, 2, 1, 2)

        # layout.addWidget(QLabel('更多使用教程 请访问'), 3, 0, 1, 2)

        bilibili_url = QLabel()
        bilibili_url.setOpenExternalLinks(True)
        bilibili_url.setText(_translate("MainWindow", "<html><head/><body><p><a href=\"https://space.bilibili.com/637783\">\
<span style=\" text-decoration: underline; color:#87CEFA;\">神君Channel:  https://space.bilibili.com/637783</span></a></p></body></html>",
                                        None))
        layout.addWidget(bilibili_url, 2, 0, 1, 4)

    def selectBackgroundColor(self):
        color = QColorDialog.getColor(self.backgroundColorLabel.text())
        if color.isValid():
            color = color.name()
            self.color.emit(color)
            self.backgroundColorLabel.setText(color)
            self.backgroundColorLabel.setStyleSheet('background-color:%s;color:%s' % (color, color))

    def setOpacity(self):
        self.opacityToken = not self.opacityToken
        if self.opacityToken:
            self.opacityButton.setStyleSheet('background-color:#3daee9')
        else:
            self.opacityButton.setStyleSheet('background-color:white')
        self.opacity.emit(self.opacityToken)

    def setTop(self):
        self.topToken = not self.topToken
        if self.topToken:
            self.stayTopButton.setStyleSheet('background-color:#3daee9')
        else:
            self.stayTopButton.setStyleSheet('background-color:white')
        self.top.emit(self.topToken)
